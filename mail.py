import smtplib
from secret_key import Key


def send_email(message, email):
    server = smtplib.SMTP_SSL('smtp.gmail.com', 465)
    server.login(Key.mail, Key.pass_word)
    server.sendmail(Key.mail, email, message)
