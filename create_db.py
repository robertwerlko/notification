import mysql
from mysql import connector
from secret_key import Key

my_db = mysql.connector.connect(
    host=Key.host,
    user=Key.user,
    password=Key.password,
    database=Key.database,
)
mycursor = my_db.cursor()

create_user_table_query = """CREATE TABLE user (id INT AUTO_INCREMENT PRIMARY KEY, user_name VARCHAR(255), phone VARCHAR(255),email VARCHAR(255))"""
create_room_table_query = """CREATE TABLE room (id INT AUTO_INCREMENT PRIMARY KEY, room_name VARCHAR(255))"""
create_order_table_query = """CREATE TABLE order (id INT AUTO_INCREMENT PRIMARY KEY, order_date TIME ,person_id  int, room_id int)"""

mycursor.execute(create_user_table_query)
mycursor.execute(create_room_table_query)
mycursor.execute(create_order_table_query)
my_db.commit()
