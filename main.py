from db import Database
from mail import send_email
from phone import send_phone


def main(order):
    model = Database()
    room = order.get('room')
    order_queryset = model.filter(table='orders', room_id=room)
    if order_queryset:
        queryset = order_queryset[0]
        msg = f"This room {order.get('room')} was booked by {queryset[1]} until Tomorrow"
        send_email(msg, order.get('email'))
        send_phone(msg, order.get('phone'))
    else:
        user = model.filter(table='user', user_name=order.get('user_name'))
        if user:
            user_id = user[0]
        else:
            user = order.copy()
            user.pop('room')
            user_instance = model.create(table='user', **user)
            user_id = user_instance[0]

        model.create(table='orders', room_id=order.get('room'), person_id=user_id[0])
        msg = f"Room number room {order.get('room')} was booked by room {order.get('user_name')} (date)"
        send_email(msg, order.get('email'))
        send_phone(msg, order.get('phone'))


if __name__ == '__main__':
    orders = {
        'user_name': 'Zafar8326',
        'phone': '+998904328324',
        'email': 'robertwerlko@gmail.com',
        'room': 1
    }
    main(orders)
