from twilio.rest import Client
from secret_key import Key


def send_phone(message, phone):
    account_sid = Key.account_sid
    auth_token = Key.auth_token

    client = Client(account_sid, auth_token)

    message = client.messages.create(
        body=message,
        from_=Key.from_,
        to=phone
        # to='+998914368835', '+998904328324'
    )

    print(message.sid)
