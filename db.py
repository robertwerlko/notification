import mysql
from mysql import connector
from secret_key import Key


class Database:
    cursor = None
    db = mysql.connector.connect(
        host=Key.host,
        user=Key.user,
        password=Key.password,
        database=Key.database,
    )

    def __init__(self):
        self.cursor = self.db.cursor()

    def all(self, table):
        sql = f"SELECT * FROM {table}"
        self.cursor.execute(sql)
        return list(self.cursor.fetchall())

    def filter(self, table, **kwargs):
        sql = f"SELECT * FROM {table}"
        for k, v in kwargs.items():
            sql = f"SELECT * FROM {table} WHERE {k}='{v}'"
        self.cursor.execute(sql)
        return list(self.cursor.fetchall())

    def create(self, table, **kwargs):
        print(kwargs.values())
        lst = []
        fields = ",".join(kwargs.keys())
        for i in kwargs.values():
            if isinstance(i, int):
                lst.append(str(i))
            else:
                lst.append(i)
        values = "','".join(lst)
        sql = f"INSERT INTO {table} ({fields}) VALUES ('{values}')"
        self.cursor.execute(sql)
        self.db.commit()
        return f"'{values}' created"

    def get(self, table, pk):
        sql = f"SELECT * FROM {table} WHERE id ={pk}"
        self.cursor.execute(sql)
        return self.cursor.fetchone()

    def delete(self, table, pk):
        sql = f"DELETE FROM {table} WHERE id = {pk}"
        self.cursor.execute(sql)
        self.db.commit()
        return f"Deleted {pk} from {table}"
